﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;




public class NodeScript : MonoBehaviour
{
    // CLASS VARIABLES ////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    #region variables

    /// <summary>
    /// NAME:
    ///     List of weights along each edge to neighbors
    ///     
    /// DESCRIPTION:
    ///     Contains the weight of the node as a float and is in the same 
    ///     index as the neighbor connection that it is linked to.
    /// NOTES:
    ///     if the neighbor is in index 1 then the edge weight to this should be placed here
    /// </summary>
    public List<float> neighborWeightList;

    /// <summary>
    /// NAME:
    ///     neighbors
    /// DESCRIPTION:
    ///     Gives references to the nodes neighbor nodes it is directly 
    ///     connected to
    /// NOTES:
    ///     N/A
    /// </summary>
    public List<GameObject> neighbors;

    /// <summary>
    /// NAME:
    ///     TurnList
    /// DESCRIPTION:
    ///     this is a bool that will hold the status of if it is my turn or my neighbors turn  
    ///     
    /// NOTES:
    ///    
    /// </summary>
    public List<bool> turnList;

   
    
    /// <summary>
    /// NAME:
    ///     shortestPath
    /// DESCRIPTION:
    ///     Stores the shortest path for each node it can reach
    /// NOTES:
    ///     // shortests paths to get to all other nodes
    ///     Indexed from end to start, includes the calling node
    /// </summary>
    public List<List<GameObject>> shortestPaths;  // shortests paths to get to all other nodes
    // game objects that holde linerenderers to draw between nodes 
    public List<GameObject> edges;
    /// <summary>
    /// Name:
    ///     flowList
    /// DESCRIPTION:
    ///     A list of strings that hold the destination node and the number of packets to be used in the flow
    /// NOTES:
    ///     Format (name of Destionation),(# of packets)
    ///     will need to use a string parse to extract data from this list 
    /// </summary>

    
    public bool inishPackets;
    public List<GameObject> flowList;
    public List<int> flowSpeedList;
    int packetFlowRate;
    public GameObject packet;
    public int LowPacketFlow = 10;
    public int MedPacketFlow = 20;
    public int HighPacletFlow = 30;
   
    public GameObject tempGO1, tempGO2;

    // this contains the same number of queues as the neighbors
    public List<Queue<GameObject>> packetBufferList = new List<Queue<GameObject>>();

    public int packetBufferQueueLength;
    public List<GameObject> received = new List<GameObject>();

    public GameManagerScr gameManager;

    bool birthPackets = true;

    #endregion

    void Start()
    {
        // gamemanager object find 
        gameManager = GameObject.Find("GM").GetComponent<GameManagerScr>();
       
        shortestPaths = new List<List<GameObject>>();
    }

    void Update()
    {
    
        if(gameManager.stepFarward)
        {
            

            //creat all packets from flows once every setFarward 
            if (birthPackets)
            {
                // Debug.Log("create packets");
                createPackets();
                birthPackets = false;
            }


            // if we have no packets to send on a line then free up that line for other node
            checkPackets();
            //check all queues in the node and send packets if line is not busy
            sendPacket();

        }

        if (!birthPackets)
        {
            if (!gameManager.stepFarward)
            {
                birthPackets = true;
            }
        }


        
    }

    
    // CLASS FUNCTIONS ////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    #region onGUI
    void OnGUI()
    {
        //
        GUI.depth = 10;
        var point = Camera.main.WorldToScreenPoint(transform.position);
        GUI.Button(new Rect(point.x, Screen.currentResolution.height-point.y - 125, 60, 30), name);
    }
    #endregion

    #region addNeighbor
    /// <summary>
    /// NAME:
    ///     addNeighbor
    /// DESCRIPTION:
    ///     Adds the given neighbor to the neighbor array
    /// DEPENDENCIES:
    ///     Neighbor must not already exist
    /// NOTES:
    ///     returns true on a successful addition, false if not
    /// </summary>
    public bool addNeighbor(GameObject newNeighbor, GameObject edge, float weight, bool yourTurn) // 
    {
        // check if already a neighbor
        if (neighbors.Find(x => x == newNeighbor) != null)
        {
            return false;
        }
        // add parameter to List
        neighbors.Add(newNeighbor); // add the new neighbor
        neighborWeightList.Add(weight); // add the new weight to the same spot in the weight list
        edges.Add(edge);
        edge.GetComponent<setEdgeScr>().setEdge(transform.position, newNeighbor.transform.position);
        edge.GetComponent<setEdgeScr>().packetOnTheLine = false;
        packetBufferList.Add(new Queue<GameObject>());

        
        turnList.Add(yourTurn);

        return true;
    }

    public void updateEdges()
    {
        // up date my edges
        for(int i = 0; i < neighbors.Count; i++)
        {
            edges[i].GetComponent<setEdgeScr>().setEdge(transform.position, neighbors[i].transform.position);
        }
    }

    #endregion
    
    #region removeNeighbor
    /// <summary>
    /// NAME:
    ///     removeNeighbor
    /// DESCRIPTION:
    ///     Removes neighbor from neighbors list
    /// DEPENDENCIES:
    ///     Neighbor must exist
    /// NOTES:
    ///     N/A
    /// </summary>
    public bool removeNeighbor(GameObject removeNeighbor)  // dont forget to remove float that coeresponds to index
    {
        // remove neighbor from the neighbors list
        int index = neighbors.FindIndex(x => x == removeNeighbor);

        neighbors.Remove(removeNeighbor);
        neighborWeightList.RemoveAt(index);
        GameObject tempGO = edges[index];
        edges.RemoveAt(index);
        Destroy(tempGO);
        return true;
    }

    #endregion
    
    #region DJAlgorithm

    /// <summary>
    /// NAME:
    ///     djAlgorithm
    ///     
    /// DESCRIPTION:
    /// 
    /// DEPENDENCIES:
    /// 
    /// </summary>
    /// <returns></returns>
    public bool DjsAlgorithm( GameObject source,  List<GameObject> ALLNodes)
    {
     // variables
        
        // visited list
        List<GameObject> visited = new List<GameObject>();                  // contains nothing
        List<GameObject> notVisitedYet = new List<GameObject>(ALLNodes);// now contains all nodes
        List<GameObject> currentPath = new List<GameObject>();              // contains nothing
        
        List<GameObject> tmpGOList = new List<GameObject>();                // tmpGOList
        bool entryFound = false;
       
        // dj algorithm - this is the first node: initilization - source node

            // add the source to the visited list and remove it from the notvisitedyetlist and the current path
            visited.Add(source); notVisitedYet.Remove(source); currentPath.Add(source);

            // for each node in the visited list // the visited list should get bigger as the for loop runs
            for(int i = 0; i < visited.Count; i++)
            {
           
                // for each neighbor
                for (int j = 0; j < currentPath[0].GetComponent<NodeScript>().neighbors.Count; j++)
                {
                   
                    
                    // copy the current path list to the tmpGOList
                    tmpGOList = new List<GameObject>(currentPath);
               
                    // add the neighbor to the front of the tmpGO list
                    tmpGOList.Insert(0, currentPath[0].GetComponent<NodeScript>().neighbors[j]);
               
                    // for each path in the pathsList 
                    for (int k = 0; k < shortestPaths.Count; k++)
                    {
                        // check to see if the target of the path is the same as ours
                        if(shortestPaths[k][0] == tmpGOList[0])
                        {
                        //debug
                       
                            
                            // then we need to check for which is shorter
                            if(Graph.calcPathWeight(shortestPaths[k]) > Graph.calcPathWeight(tmpGOList))
                            {
                            Debug.Log("This is the inside of the calcPathweight comparisson");
                            // then they need to be swapped
                                // remove the old path
                                shortestPaths.Remove(shortestPaths[k]);
                                // add the new path
                                shortestPaths.Add(tmpGOList);
                                break; // this will break the for loop and will only account for having one path to each node
                            }
                        }
                       
                    }

                    // set before using every round
                    entryFound = false;

                    // go through the pathslist again
                    for(int k = 0; k < shortestPaths.Count; k++)
                    {
                        // look for the top node and compare
                        if(shortestPaths[k][0] == tmpGOList[0])
                        {

                            // we found a match and we are done now
                            entryFound = true;
                            break;
                        }

                    }

                    // if we did
                    if(!entryFound)
                    {
                        // if the node is not this node
                        if (tmpGOList[0] != this.gameObject )
                        {
                            // insert the tmpGOLList at the end of the pathslist

                            shortestPaths.Add(tmpGOList);
                            // Debug.Log("ADDED STUFF TO the Shortest paths list");
                        }
                    }
                                        
                } // all neighbors have been gone through and added appropriately!

                // find which neighbor to use as the next call

                    // create a new paths list to play with
                    List<List<GameObject>> tmpLofL = new List<List<GameObject>> (shortestPaths);

           

            // go through the visited list
            for ( int j = 0; j < visited.Count; j++)
                    {
                        // go through the tmpLofL for each visited GO
                        for(int k = 0; k < tmpLofL.Count; k++)
                        {
                            // compare if this is the list we are looking for
                            if( tmpLofL[k][0] == visited[j] )
                            {
                                // use the k find the index of the found object list
                                tmpLofL.Remove(tmpLofL[k]);

                                // break
                                break;
                            }
                        } 
                        
                    }   // now we have the tmpLofL will have all paths that have nodes that havent been visited

                    // find the shortest path that is in the tmpLofL
                    // prep for loop
                    if (tmpLofL.Count > 0)
                    {
                        // set the shortest path to be the first path
                        tmpGOList = new List<GameObject>(tmpLofL[0]);

                        // for each path in the tmpLofL list of lists
                        for (int j = 1; j < tmpLofL.Count; j++)
                        {
                            // if the new path is shorter than the old path
                            if (Graph.calcPathWeight(tmpLofL[j]) < Graph.calcPathWeight(tmpGOList)) ;
                            {
                                // keep the new path
                                tmpGOList = tmpLofL[j];
                            }
                        }// now we know that the tmpGOList is the next shortest path 
                         // we now must run this on that node, set the visited lists next node to be tmpGOList top node
                        if (tmpGOList[0] != this.gameObject)
                        {
                            visited.Add(tmpGOList[0]);
                        }
                        // remove the node from the nonvisited yet list
                        notVisitedYet.Remove(tmpGOList[0]);

                        // save the selected path as the current path
                        currentPath = tmpGOList;
                    } 
                    
                       
            }
                      

        return true;
    }



    #endregion

    #region findWayAroundNode


    public List<List<GameObject>> findWayAroundNode(GameObject skipThisNode)
    {

        // variables
        List<GameObject> limitedNodeList = new List<GameObject> (Graph.allNodes);
        List<List<GameObject>> tmpLofL = new List<List<GameObject>>(); 


        // remove the node from the allnodes list
        limitedNodeList.Remove(skipThisNode);

        // then use the new limitedGraph to find shortest paths
        DjsAlgorithm(this.gameObject, limitedNodeList);

        // we have the shortest way around if we want it
        for(int i = 0; i < shortestPaths.Count; i++)
        {
            // add the list from each path
            tmpLofL.Add(shortestPaths[i]);

        }
        
        // now rerun dj with original data
        DjsAlgorithm(this.gameObject, Graph.allNodes);

        // now all damage is done! we are back to a working state 

        // return the list of paths of gamebojects
        return tmpLofL;
    }


    #endregion

    #region sendPacket
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public bool sendPacket()
    {
        GameObject tmpGO;
        // any line that is not busy i should send down
        for (int i = 0; i < neighbors.Count; i++)
        {
            // if line is not busy and if its my turn and there is something to send
            if (turnList[i])
            {
                if(!edges[i].GetComponent<setEdgeScr>().packetOnTheLine)
                {
                    if (packetBufferList[i].Count > 0)
                    {
                        // dequeue the packet
                        tmpGO = packetBufferList[i].Dequeue();

                        // set to active to be sure
                        tmpGO.SetActive(true);

                        // velocity of the packet is distace over weight
                        tmpGO.GetComponent<PacketScript>().currentVelocity = (Vector3.Distance(gameObject.transform.position, neighbors[i].transform.position) - .13f) / neighborWeightList[i];

                        // set this node as the last node
                        tmpGO.GetComponent<PacketScript>().lastNode = gameObject;

                        // we are off the node
                        tmpGO.GetComponent<PacketScript>().onNode = false;

                        // set paths to busy
                        int index = neighbors[i].GetComponent<NodeScript>().neighbors.FindIndex(x => x == gameObject);
                        
                        // set the neighbors path to us as being busy
                        edges[i].GetComponent<setEdgeScr>().packetOnTheLine = true;
                        
                        Debug.Log("the packet is on the line " + gameObject.name);

                        // place the packet on the line
                        tmpGO.transform.position = this.gameObject.transform.position;

                        // turn is now my neighbors turn
                        turnList[i] = false;

                        neighbors[i].GetComponent<NodeScript>().turnList[index] = true;

                    }
                }
            }

        }

        return false;
    }
    #endregion
    
    #region receivePacket
    /// <summary>
    /// 
    /// </summary>
    /// <param name="packetRec"></param>
    /// <returns></returns>
    public bool receivePacket(GameObject packetRec) // returns false if dropped
    {


        // find the index of us in the neighobr list 
        packetRec.GetComponent<PacketScript>().onNode = true;
        

        /*//// This is for debugging the lastnodes neighbors
        for (int i = 0; i < packetRec.GetComponent<PacketScript>().lastNode.GetComponent<NodeScript>().neighbors.Count; i++)
        {
            Debug.Log(" Neigbors in the list :" + packetRec.GetComponent<PacketScript>().lastNode.GetComponent<NodeScript>().neighbors[i].name + " US :"  + gameObject.name);
        }
        */
        int fromNodesIndex = neighbors.FindIndex(x => x == packetRec.GetComponent<PacketScript>().lastNode);
        // if the packet is at its destination
        if (packetRec.GetComponent<PacketScript>().Path[0] == this.gameObject )
        {
            // this packet is at its destination
            // add to the received list and deactivate
            received.Add(packetRec);
            // turn off
            packetRec.SetActive(false);
           

            int fromNodesIndexOfThisNode = packetRec.GetComponent<PacketScript>().lastNode.GetComponent<NodeScript>().neighbors.FindIndex(x => x == gameObject);

            // find the bool in our neighbor and set it to false: no packet on the line

            // find the position of our neighbors in the neighbors list


            // clear the packet on the line bool at the index
            edges[fromNodesIndex].GetComponent<setEdgeScr>().packetOnTheLine = false;
            Debug.Log("we have changed packet on the line in the edge to false :"+edges[fromNodesIndex].GetComponent<setEdgeScr>().packetOnTheLine);
            // chage the lastnode in the packet to be this node
            packetRec.GetComponent<PacketScript>().lastNode = gameObject;
            return true;
        }


        // add to the buffer if the buffer is not full
        else if( packetBufferQueueLength <= packetBufferList[fromNodesIndex].Count )
        {
            // drop packet
            packetRec.GetComponent<PacketScript>().dropped(); // packet is still active

            // clear the packet on the line bool at the index
            edges[fromNodesIndex].GetComponent<setEdgeScr>().packetOnTheLine = false;

            packetRec.GetComponent<PacketScript>().lastNode = gameObject;
            
            // return true
            return false;
        }
        // else add to the queue
        else
        {
            // add to the send queue
            int ourTargetIndexInThePacketPath = (packetRec.GetComponent<PacketScript>().Path.FindIndex(x => x == gameObject))-1;
            packetBufferList[ourTargetIndexInThePacketPath].Enqueue(packetRec);
                       
            // turn off
            packetRec.SetActive(false);
           
            // return true

            int thereIndex = packetRec.GetComponent<PacketScript>().lastNode.GetComponent<NodeScript>().neighbors.FindIndex(x => x == gameObject);

            // find the bool in our neighbor and set it to false: no packet on the line

            // find the position of our neighbors in the neighbors list


            // clear the packet on the line bool at the index
            edges[fromNodesIndex].GetComponent<setEdgeScr>().packetOnTheLine = false;
            Debug.Log("we have changed packet on the line in the edge to false :" + edges[fromNodesIndex].GetComponent<setEdgeScr>().packetOnTheLine);
            packetRec.GetComponent<PacketScript>().lastNode = gameObject;
            return true;
        }
        
    }
    #endregion

    #region checkPackets
    void checkPackets()
    {
        

        for (int i = 0; i < neighbors.Count; i++)
        {
            // if line is not busy and if its my turn and there is something to send
            if (packetBufferList[i].Count <= 0)
            {
                
                int index = neighbors[i].GetComponent<NodeScript>().neighbors.FindIndex(x => x == gameObject);
                turnList[i] = false;
                neighbors[i].GetComponent<NodeScript>().turnList[index] = true;
            }
        }
    }
    #endregion

    #region addFlow
    public void addFlow(string dest, string nameOfBtn)
    {

        GameObject tempBtn = GameObject.Find(nameOfBtn);
        GameObject tempDest = GameObject.Find(dest);

        if (flowList.Find(x => x == tempDest) == null)
        {
            //tempBtn.transform.GetChild(0).GetComponent<Image>().color = Color.red;
            flowList.Add(tempDest);
            flowSpeedList.Add(LowPacketFlow);
            // change color of image on button
            // show buttons high medium low 
            tempBtn.transform.GetChild(1).gameObject.SetActive(true);
            tempBtn.transform.GetChild(2).gameObject.SetActive(true);
            tempBtn.transform.GetChild(3).gameObject.SetActive(true);

        }    
        else
        {
            int tempIndex = flowList.FindIndex(x => x == tempDest);
            flowList.Remove(tempDest);
            flowSpeedList.RemoveAt(tempIndex);
            // change color of image on button 
            // hide buttons high medium low 
            tempBtn.transform.GetChild(1).gameObject.SetActive(false);
            tempBtn.transform.GetChild(2).gameObject.SetActive(false);
            tempBtn.transform.GetChild(3).gameObject.SetActive(false);
        }
        
    }
    #endregion

    #region createPackets
    public void createPackets()
    {
       
        for (int i = 0; i < flowList.Count; i++)
        {

            if(flowSpeedList[i] > 0)
            {
                // find shortest path dest that matches flowlist[i]
                int j = 0;
                // Debug.Log(shortestPaths[j][0].name + "  " + flowList[i].name);

                    // find the gameobject that is the dereferenced flowlist in the haed position of the shortests paths list 
                    while (shortestPaths[j][0] != flowList[i])
                    {  
                        j++;
                    }
                
                // pull count-2 from the list as the queue
                tempGO1 = shortestPaths[j][shortestPaths[j].Count - 2];

                // find neighbor that matches 
                int tempIndex = neighbors.FindIndex(x => x == tempGO1);
                
                // check its queue to see if its full if not 
                if (packetBufferList[tempIndex].Count < packetBufferQueueLength)
                {
                    
                    // create packet for queue
                    
                        tempGO2 = (GameObject)Instantiate(packet, new Vector3(0, 0, 0), Quaternion.identity);
                        // if statment for debug to check if packet path has been set 
                        if (!tempGO2.GetComponent<PacketScript>().setPath(gameObject, flowList[i]))
                        {
                            Debug.Log("Packet path was not set");

                        }

                        tempGO2.name = gameObject.name + " " + flowList[i].name + " " + flowSpeedList[i];
                        tempGO2.transform.parent = gameObject.transform;
                        // it was created in the node so it is on a node
                        tempGO2.GetComponent<PacketScript>().onNode = true;
                        // 
                        tempGO2.GetComponent<PacketScript>().lastNode = gameObject;

                        tempGO2.SetActive(false);

                        // enqeue
                        packetBufferList[tempIndex].Enqueue(tempGO2);
                        flowSpeedList[i]--;
                    
                }


            }

        }
    }
    #endregion

    #region setFlowRate
    public void setFlowRate(int flowRate, string destGO)
    {
        tempGO1 = GameObject.Find(destGO);

        int tempIndex = flowList.FindIndex(x => x == tempGO1);

        if (flowRate == 0)
        {
            flowSpeedList[tempIndex] = LowPacketFlow;
        }
        else if (flowRate == 1)
        {
            flowSpeedList[tempIndex] = MedPacketFlow;
        }
        else
        {
            flowSpeedList[tempIndex] = HighPacletFlow;
        }

    }
    #endregion


}

