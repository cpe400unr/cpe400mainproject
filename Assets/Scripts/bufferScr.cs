﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class bufferScr : MonoBehaviour {
    public int bufferSize;
    public GameObject contaner;
    public List<GameObject> bufferList;
    private float xpos,
                  ypos;
    public float spacing = 0.3f;

	// Use this for initialization
	void Start () {
        bufferList = new List<GameObject>();
        xpos = gameObject.transform.position.x;
        ypos = gameObject.transform.position.y;

	    for(int i = 0; i < bufferSize; i++)
        {
            bufferList.Add((GameObject)Instantiate(contaner, new Vector3(xpos, ypos, 0), Quaternion.identity));
            xpos -= spacing;
            bufferList[i].transform.parent = gameObject.transform;

        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
