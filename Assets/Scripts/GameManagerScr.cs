﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameManagerScr : MonoBehaviour
{
    #region variables
    // variables
    public static bool simulationStart; // 
     
    public int simulationSteps;         // this is the number of total steps in the simulation
    public float timeStep;              // how fast is our step
    public float simulationTimeTotal;   // total simulation time that has passed

    public bool stepFarward;        // this is the running bool that will tell the gameobject to simulate or not the next step
    public float stopTime;          // this is when we chage the stepFarward to false
    

    #endregion

    // Use this for initialization
    void Start ()
    {
        simulationStart = false;

        Graph.allNodes = new List<GameObject>();

        simulationSteps = 0;
        timeStep = 1f;
        simulationTimeTotal = 0f;
        
    }
	
	// Update is called once per frame
	void Update ()
    {
	
        if(stepFarward)
        {
            // make sure we only do this step
            if (Time.timeSinceLevelLoad < stopTime)
            {

            }
            // else stop 
            else
            {
                stepFarward = false;
            }

            // time moves
                          

        }


	}

    public void StepF()
    {
        // timer is set
        stopTime = Time.timeSinceLevelLoad + timeStep;

        // set the stepfarward to be true
        stepFarward = true;
        // 

    }





}
