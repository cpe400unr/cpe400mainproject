﻿using UnityEngine;
using System.Collections;

public class fadeOutScr : MonoBehaviour
{

    public float minimum = 1f;
    public float maximum = 0.0f;
    public float duration = 10f;
    private float startTime;
    private bool fade = false;
    
    void Start()
    {
        
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(2))
        {
            fade = true;
            startTime = Time.time;
        }

        if(fade)
        {
            float t = (Time.time - startTime) / duration;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, Mathf.SmoothStep(1, 0, t));
        }

    }
}
