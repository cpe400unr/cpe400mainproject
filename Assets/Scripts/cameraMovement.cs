﻿using UnityEngine;
using System.Collections;

public class cameraMovement : MonoBehaviour {

    public int speed = 2;
    public static bool zooming { get; private set; }
    public static float deltaZoom;
    private Vector2 lastRelativePosition = new Vector2(0, 0);

    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update() {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(-touchDeltaPosition.x * speed * Time.deltaTime, -touchDeltaPosition.y * speed * Time.deltaTime, 0);
        }

        if (Input.touchCount == 2)
        {
            if (!zooming)
            {
                zooming = true;
                lastRelativePosition = Input.GetTouch(1).position - Input.GetTouch(0).position;
                Debug.Log("Zooming");
            }
            else
            {
                Vector2 relativePosition = Input.GetTouch(1).position - Input.GetTouch(0).position;
                deltaZoom = relativePosition.magnitude - lastRelativePosition.magnitude;
                lastRelativePosition = Input.GetTouch(1).position - Input.GetTouch(0).position;
            }

            if (transform.position.z > -20 && transform.position.z < 20)
            {
                transform.Translate(0, 0, deltaZoom * 1 * Time.deltaTime);

            }

        }
        else
        {
            zooming = false;
        }
    }
}
