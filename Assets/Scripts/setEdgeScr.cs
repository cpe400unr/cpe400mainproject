﻿using UnityEngine;
using System.Collections;

public class setEdgeScr : MonoBehaviour {
    public Color c1 = Color.green;
    public Color c2 = Color.blue;
    public int lengthOfLineRenderer = 20;
    int vertCount = 0;
    public LineRenderer mainLine;
    public Material redLine;
    bool highlight = false;
    Vector3 startPos;
    Vector3 endPos;
    Vector3 printPos;
    public bool packetOnTheLine = false;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setEdge(Vector3 source, Vector3 dest)
    {
        mainLine = GetComponent<LineRenderer>();
        if (!highlight)
        {
            mainLine.material = new Material(Shader.Find("Particles/Additive"));
        }
        else
        {
            mainLine.material = redLine;
        }
        mainLine.SetColors(c1, c2);
        mainLine.SetWidth(0.04F, 0.04F);
        mainLine.SetPosition(0, source);
        mainLine.SetPosition(1, dest);

        startPos = source;
        endPos = dest;

    }

    public void upDateEdge(Vector3 source, Vector3 dest)
    {
        mainLine.SetPosition(0, source);
        mainLine.SetPosition(1, dest);

        startPos = source;
        endPos = dest;
    }

    public void setColorRed()
    {
        mainLine.material = redLine;
        highlight = true;
    }

    public void setColorBack()
    {
        mainLine.material = new Material(Shader.Find("Particles/Additive"));
        highlight = false;
    }

    void OnGUI()
    {
        printPos =  startPos - endPos;
        printPos = printPos / 2;
        var point = Camera.main.WorldToScreenPoint(printPos);
        GUI.Button(new Rect(point.x, Screen.currentResolution.height - point.y, 60, 30), gameObject.name);

    }
}
