﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIManagerScr : MonoBehaviour {
    
    public GameObject mainCameraTarget; //GO that the camera follows when moved by clicking

    // Inish Nodes 
    GameObject inishNodesMenu; //menu that allows user to set a certain amount of nodes on the scene
    string numberOfNodes = "0"; // string that is used to record users input for inishNodeMenu

    //main menu objs
    GameObject mainMenu; //menu that allows user to pick nodes and start the simulator
    GameObject mainMenuList; // list of all nodes on the scene
    GameObject startSim; //button that starts the simulation
    GameObject stopSim; //buttons that stops the simulation

    //node menu objs 
    GameObject nodeMenu; // menu that allows user to set connections\flows to other nodes 
    GameObject nodeMenuTitle; // title of node menu  
    GameObject connectionsList; // list of nodes that are connected to the current node 
    GameObject allOtherNodesList; // list of all other nodes that are not connected to the current node
    public GameObject flowList; // flow holds the traget gameobject for the current node 
    GameObject currentNode; // nodemenu displays all info about this currentNode
                            // currentNode is selected by clicking on nodes on the scene 
    public List<GameObject> connectionsBtnList; //buttons to handle the connecting of all nodes to all other nodes

    //referances
    // set are all droped in from the unity inface 
    public GameObject GUINode; // prefab of the GUINode
    public GameObject GUIEdge; // prefab of the GUIEdge 
    public GameObject mainMenuNodeBtn; // prefab the main menu node buttons
    public GameObject connectionNodeBtn; //prefab of the connections Buttons
    public GameObject FlowBtn; //prefab of the flow buttons 


    GameObject tempGO;
    GameObject tempBtn;


    public float spacingOfNodes = 2; // the spacing of the nodes when they are created on the scene 

    // Use this for initialization
    void Start () {
        //getting all the game objects on the scene in to there prespective values 
        inishNodesMenu = GameObject.Find("InishNodesMenu");
        mainMenu = GameObject.Find("MainMenu");
        mainMenuList = GameObject.Find("MainMenuList");
        startSim = GameObject.Find("StartSim");
        stopSim = GameObject.Find("StopSim");
        nodeMenu = GameObject.Find("NodeMenu");
        nodeMenuTitle = GameObject.Find("NodeMenuTitle");
        connectionsList = GameObject.Find("ConnectionsList");
        allOtherNodesList = GameObject.Find("AllOtherNodesList");
        flowList = GameObject.Find("FlowNodesList");
        connectionsBtnList = new List<GameObject>();


        //show the first menu only 
        inishNodesMenu.SetActive(true);
        //hide the menues that are not needed yet 
        mainMenu.SetActive(false);
        nodeMenu.SetActive(false);
        stopSim.SetActive(false);

    }
	

    // all functions needed to run the inishNodeMenu
    // inishNodeMenu will create the number of nodes requested by the user
    #region inishNodeMenu
    // function that is called by input box on inishNodeMenu
    public void getNumberOfNodes( string number)
    {
        numberOfNodes = number;
    }

    // funciton takes numberOfNodes and creates inishal nodes as well as the 
    // corisponding buttons that are needed to minipulate the nodes on the scene
    public void creatNodes( )
    {
        int divisor = 1, // number used to build the rows of the nodes when they are first printed
            numOfN, // number of total nodes needed
            maxX, // number of colombs that will be printed for the nodes 
            yIndex = 0, // yindex for printing nodes 
            xIndex = 0; // xindex for printing nodes 

        // convert whole string in to int 
        numOfN = int.Parse(numberOfNodes);

        // make sure its a number if not they must put in a number

        // if string is only one diget divide by 2
        // if string is at least 2 digits then parse first char off string convert to an int and add 2
        if (numOfN <= 0)
        {
            // then value hasnt been set yet or been set to a negative value
        }
        else if (numberOfNodes.Length > 1)
        {
            int tempNum;
            // convert text input to number
            tempNum = (numberOfNodes[0] - '0');
            // set up the number of rows that will be printed for the nodes 
            divisor = tempNum + 2;
        }
        else
        {
            divisor = 2;
        }
       
        // create number of colobms needed to print nodes 
        maxX = numOfN / divisor;

        // then run the double for loop and print out a grid
        for (int i = 0; i < numOfN; i++)
        {
            if(xIndex > maxX)
            {
                // move row down a line 
                xIndex = 0;
                yIndex++;
            }

            //create the node for sence 
            tempGO = (GameObject)Instantiate(GUINode, new Vector3(xIndex*spacingOfNodes, yIndex*spacingOfNodes, 0), Quaternion.identity);
            tempGO.tag = "Node";
            tempGO.name = "Node " + i;
            Graph.addNode(tempGO);

            //main menu buttons
            string localI = i.ToString();
            //create the button
            tempBtn = (GameObject)Instantiate(mainMenuNodeBtn);
            //set text of the button
            tempBtn.transform.GetChild(0).GetComponent<Text>().text = tempGO.name;
            // create an onclick event for the button so that it can find its node on the scene
            UnityEngine.Events.UnityAction action1 = () => { callNodeMenu("Node "+localI); };
            // add that onclick event to the button
            tempBtn.GetComponent<Button>().onClick.AddListener(action1);
            // but that button under the list of GM in the main node menu
            tempBtn.transform.parent = mainMenuList.transform;

            //create connectNodeBtns
            //create a new gameobject from prefab
            tempBtn = (GameObject)Instantiate(connectionNodeBtn);
            // name the gameobject
            tempBtn.name = "Btn,Node " + localI;
            // set parent of gameobject, which is a button, to allOtherNodesList 
            tempBtn.transform.parent = allOtherNodesList.transform;
            // set the name of the button that the user will see
            tempBtn.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = tempGO.name;
            // create an action that can be set to a listener
            UnityEngine.Events.UnityAction action2 = () => { moveListing("Btn,Node " + localI); };
            // take action2 and set it as a listoner to the button that is creating a connection for the selected node
            tempBtn.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(action2);
            // add the button to list of connectionBtnList so that they can be moved between 
                // the allothernodeslist and connectionlist for the user 
            connectionsBtnList.Add(tempBtn);

            //create a Flow Btn 
            // this will allow the user to start a packet flow for the selected node
            tempBtn = (GameObject)Instantiate(FlowBtn);
            // name the button
            tempBtn.name = "Flo,Node " + localI;
            // set the button in to the list 
            tempBtn.transform.parent = flowList.transform;
            // change the name of the button that the user sees 
            tempBtn.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = tempGO.name;

            // add listoner to main button
            UnityEngine.Events.UnityAction action3 = () => { inishFlow("Flo,Node " + localI); };  
            tempBtn.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(action3);

            // add listoner to low speed button
            UnityEngine.Events.UnityAction action4 = () => { setFlowLow("Flo,Node " + localI); };
            tempBtn.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(action4);

            // add listoner to Med speed button
            UnityEngine.Events.UnityAction action5 = () => { setFlowMed("Flo,Node " + localI); };
            tempBtn.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(action5);

            // add listoner to low speed button
            UnityEngine.Events.UnityAction action6 = () => { setFlowHigh("Flo,Node " + localI); };
            tempBtn.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(action6);

            xIndex++;
        }

        // once the inishNodeMenu is finished close the menu and open the mainMenu for the user  
        mainMenu.SetActive(true);
        inishNodesMenu.SetActive(false);
    }
    #endregion

    #region MainMenu
    public void callNodeMenu(string nodeName)
    {
        //inishlize node menu 
            // read which neigbhors it has and put those in the connection list 
            // and put all else in the allothernodes list
        currentNode = GameObject.Find(nodeName);

        nodeMenuTitle.GetComponent<Text>().text = nodeName;

        
        
        //set up nodeMenu 
        // put all buttons back in to all other nodes list 
        for(int i = 0; i < connectionsBtnList.Count; i++)
        {
            //extract the name of node from the button name in order to remove that nodes button from the list
            // so that the node its self can not also be its neighbor. 
            string[] tempString = connectionsBtnList[i].name.Split(',');
            string currentNodeSelected = tempString[1];

            connectionsBtnList[i].SetActive(true);

            connectionsBtnList[i].transform.parent = allOtherNodesList.transform;

            // set current node button to false so that it can not be seem
            if (currentNode.name == currentNodeSelected)
            {
                connectionsBtnList[i].SetActive(false);
            }
        }

        // find neighbors and put them back in to connectionslist 
        for(int i = 0; i < currentNode.GetComponent<NodeScript>().neighbors.Count; i++)
        {
            for ( int j = 0; j < connectionsBtnList.Count; j++)
            {
                string[] tempString = connectionsBtnList[j].name.Split(',');
                string connectionListNode = tempString[1];

                if (connectionListNode == currentNode.GetComponent<NodeScript>().neighbors[i].name)
                {
                    connectionsBtnList[j].transform.parent = connectionsList.transform;
                }
            }
        }

        // set all buttons in listes in order from there numbers 
        connectionsBtnList = Graph.SortByName(connectionsBtnList);

        //highlight all shortest paths 
        if(GameManagerScr.simulationStart)
        {
            // remove all edge highlights 
            clearAllEdges();
            // go through shortest paths of current node and highlight all paths 
            for (int i = 0; i < currentNode.GetComponent<NodeScript>().shortestPaths.Count; i++)
            {
                highlightPath(currentNode.GetComponent<NodeScript>().shortestPaths[i]);
            }
        }

        nodeMenu.SetActive(true);

    }

    void addNode()
    {
        //make this call a listoner 
    }

    public void startSimulation()
    {
        GameManagerScr.simulationStart = true;
        Graph.RunDjsAlgorithmOnGraph();

        for (int i = 0; i < currentNode.GetComponent<NodeScript>().shortestPaths.Count; i++)
        {
            highlightPath(currentNode.GetComponent<NodeScript>().shortestPaths[i]);
        }

        for( int i = 0; i < Graph.allNodes.Count; i++)
        {
            Graph.allNodes[i].GetComponent<CircleCollider2D>().radius = 0.03f;
        }

        stopSim.SetActive(true);
        startSim.SetActive(false);
    }

    public void stopSimulation()
    {
        GameManagerScr.simulationStart = false;
        clearAllEdges();
        stopSim.SetActive(false);
        startSim.SetActive(true);
    }

    public void highlightPath(List<GameObject> shortPath)
    {
        string nameOfEdge;

        for(int i = 0; i < shortPath.Count-1; i++)
        {
            nameOfEdge = nameEdge(shortPath[i], shortPath[i + 1]);
            tempGO = GameObject.Find(nameOfEdge);
            tempGO.GetComponent<setEdgeScr>().setColorRed();
        }
    }

    public void clearAllEdges()
    {
        for (int i = 0; i < Graph.allNodes.Count; i++)
        {
            for (int j = 0; j < Graph.allNodes[i].GetComponent<NodeScript>().edges.Count; j++)
            {
                Graph.allNodes[i].GetComponent<NodeScript>().edges[j].GetComponent<setEdgeScr>().setColorBack();
            }
        }
    }

    #endregion

    #region NodeMenu
    // if Box is checked then load it in to the connections list and make input box active


    void moveListing(string currentBtn)
    {
        string[] tempString = currentBtn.Split(',');
        string destination = tempString[1];
        int tempWeight;

        GameObject tempCurrentBtn = GameObject.Find(currentBtn);
        GameObject destGO = GameObject.Find(destination);

        if (tempCurrentBtn.transform.parent.name == "AllOtherNodesList")
        {
            tempCurrentBtn.transform.parent = connectionsList.transform;
            
            if( int.TryParse(tempCurrentBtn.transform.GetChild(1).GetComponent<InputField>().text, out tempWeight))
            {
                tempGO = (GameObject)Instantiate(GUIEdge, new Vector3(0, 0, 0), Quaternion.identity);
                tempGO.name = nameEdge(currentNode, destGO);
                currentNode.GetComponent<NodeScript>().addNeighbor(destGO, tempGO, tempWeight, true);
                destGO.GetComponent<NodeScript>().addNeighbor(currentNode, tempGO, tempWeight, false );
                

            }
            else
            {
                tempGO = (GameObject)Instantiate(GUIEdge, new Vector3(0, 0, 0), Quaternion.identity);
                tempGO.name = nameEdge(currentNode, destGO);
                currentNode.GetComponent<NodeScript>().addNeighbor(destGO, tempGO, 1, true);
                destGO.GetComponent<NodeScript>().addNeighbor(currentNode, tempGO, 1, false);

            }
            
        }
        else
        {
            tempCurrentBtn.transform.parent = allOtherNodesList.transform;
            currentNode.GetComponent<NodeScript>().removeNeighbor(destGO);
            destGO.GetComponent<NodeScript>().removeNeighbor(currentNode);
        }
    }

    void inishFlow(string currentFlowBtn)
    {
        // get destionation 
        string[] tempString = currentFlowBtn.Split(',');
        string destination = tempString[1];
      

        GameObject tempCurrentBtn = GameObject.Find(currentFlowBtn);

        currentNode.GetComponent<NodeScript>().addFlow(destination, currentFlowBtn);
        

    }

    public void setFlowLow(string currentFlowBtn)
    {
        string[] tempString = currentFlowBtn.Split(',');
        string destination = tempString[1];

        currentNode.GetComponent<NodeScript>().setFlowRate(0, destination);
    }

    public void setFlowMed(string currentFlowBtn)
    {
        string[] tempString = currentFlowBtn.Split(',');
        string destination = tempString[1];

        
        currentNode.GetComponent<NodeScript>().setFlowRate(1, destination);
    }

    public void setFlowHigh(string currentFlowBtn)
    {
        string[] tempString = currentFlowBtn.Split(',');
        string destination = tempString[1];

        currentNode.GetComponent<NodeScript>().setFlowRate(2, destination);
    }

    string nameEdge(GameObject one, GameObject two)
    {
        string[] oneArray = one.name.Split(' ');
        string oneValue = oneArray[1];
        string[] twoArray = two.name.Split(' ');
        string twoValue = twoArray[1];

        if(int.Parse(oneValue) > int.Parse(twoValue))
        {
            return "E," + oneValue + "," + twoValue;
        }
        else
        {
            return "E," + twoValue + "," + oneValue;
        }

        return "Nothing";
    }


    #endregion


}
