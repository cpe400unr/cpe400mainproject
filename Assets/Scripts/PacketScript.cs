﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PacketScript : MonoBehaviour
{
    #region variables
    // variables
    // gamemanager
    public GameObject       gameManager;                          // this is the game manager
    public GameManagerScr   gameManagerScript;                // this is the game manager script reference
    public List<GameObject> Path;                           // this will have the list of the path that the packet needs to take
    public bool             onNode;                  // starts true because the packet is created in a node 
    public GameObject       lastNode;                       // this will be the last node that the gameobject was on
    public GameObject       nextNode;                       // this is the next node
    public static float     weightPerTick;                 // this is the intiger of the transmission that we are going to be using
    public GameObject       sourceNode;                     // where the packet was born
    public GameObject       targetNode;                     // where the packet is going
    public float            currentVelocity;                // this is the current velocity of the packet betwen this or last set of nodes

    // Error Flags
    public bool PathFound = false;


    #endregion

    #region Start
    // Use this for initialization
    void Start ()
    {
        // find gamemanager for the reference to the gamemanager script
        gameManager = GameObject.Find("GM");
        gameManagerScript = gameManager.GetComponent<GameManagerScr>();
        

        // packet instantiation
       // Path = new List<GameObject>(); // we will have a path
        // PathFound = false;             // set the pathfound bool to false because we dont have a path yet
	}
    #endregion

    #region Update
    // Update is called once per frame
    void Update ()
    {
        if (gameManagerScript.stepFarward)
        {
            // anaimate packet if being animated
            if(!onNode)
            {
                    // set the speed using the velocity relative
                    float speed = Time.deltaTime * currentVelocity;
                // move the packet!
                // 

                this.gameObject.transform.position = Vector3.MoveTowards(transform.position, Path[Path.FindIndex(x => x == lastNode) - 1].transform.position, speed);
            }


        }
    }
    #endregion
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (lastNode.GetComponent<Collider2D>() != other)
        {
            other.gameObject.GetComponent<NodeScript>().receivePacket(this.gameObject);
            Debug.Log("packet received");
        }
    }
    



    #region setPAth
    /// <summary>
    ///     NAME:
    ///         setPath
    /// PARAMETERS:
    ///         sourceNode, targetNode        
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool setPath( GameObject source, GameObject target)
    {
        // variables
        
        sourceNode = source; targetNode = target; // set source and target node for this instance
        // for 
        
        for(int i = 0; i < source.GetComponent<NodeScript>().shortestPaths.Count; i++)
        {
           // Debug.Log("Shortest paths :" + i);
            if (source.GetComponent<NodeScript>().shortestPaths[i][0] == target)
            {
                //Debug.Log("target found");   
                for (int j = 0; j < source.GetComponent<NodeScript>().shortestPaths[i].Count; j++)
                {
                    // this is the correct target path
                    //Debug.Log("gameobjects in the shortest path :" + source.GetComponent<NodeScript>().shortestPaths[i][j].name);
                    Path.Add(source.GetComponent<NodeScript>().shortestPaths[i][j]);
                    //Debug.Log("current path " + Path[j].name);
                }
                PathFound = true; // set the classes path found to true
                return true; // we found a path
            }
        }
        PathFound = false; // set the classes pathfound to false
        return false; // we did not find a path
    }

    #endregion

    

    public void dropped()
    {
        // this packet was dropped!
    }



}
