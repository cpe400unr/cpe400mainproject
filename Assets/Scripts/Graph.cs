﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Graph
{
    // CLASS VARIABLES ////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    #region variables
    
    public static List<GameObject> allNodes; // this will be handed to the graph by the front end, with all nodes initialized and edges weighted

    #endregion
    

    // CLASS FUNCTIONS ////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    #region addNode
    /// <summary>
    /// NAME:
    ///     addNode
    /// DESCRIPTION:
    ///     Adds the given neighbor to the allNodes array
    /// DEPENDENCIES:
    ///     Neighbor must not already exist
    /// NOTES:
    ///     returns true on a successful addition, false if not
    ///     Must be run whenever a node is added to the UI
    /// </summary>
    
    public static bool addNode(GameObject newNode)   // we should be handed a gameobject list with the nodes in the 
    {
        // check if node already exists
        if (allNodes.Find(x => x == newNode) != null)
        {
            // node already exists, return failure
            return false;
        }

        // Add node to all nodes
        allNodes.Add(newNode);

        // Check if node was added
        if (allNodes.Find(x => x == newNode) != null)
        {
            // node found, return success
            return true;
        }

        // node not found
        else
        {
            // return failure
            return false;
        }
    }
    #endregion
    
    #region DJ's Algorithm Master

    /// <summary>
    /// NAME:
    ///     RunDjsAlgorithmOnGrap
    /// DESCRIPTION:
    ///    This function should be called in order to start the Dijkstra's Algorithm
    /// 
    /// DEPENDENCIES:
    ///    This needs a gameobject that has a "NodeScript" attached to it
    /// NOTES:
    ///    This function is the daddy function for the dj algorithm that is located in each node
    /// </summary>
    public static bool RunDjsAlgorithmOnGraph()
    {
        // local variables
        List<GameObject> tmpList = new List<GameObject>();

        // call Dj's Algorithm for each node in the graph
        for (int i = 0; i < allNodes.Count; i++)
        {
           
            if (!allNodes[i].GetComponent<NodeScript>().DjsAlgorithm(allNodes[i], allNodes))
            {
                // dj returned false
                

                // return false if any nodes returned false
                return false;
            }
       
        }

        
        /*
        // debug consol output for the shortest paths list ///////////////
        for(int i = 0; i < allNodes.Count; i++)
        {
            // inside node 
            Debug.Log("inside the node: " + allNodes[i].name);
            Debug.Log("Count of the paths in the shortests paths list "+ allNodes[i].GetComponent<NodeScript>().shortestPaths.Count);


            // go through the list of lists of shortests paths
            for (int j = 0; j < allNodes[i].GetComponent<NodeScript>().shortestPaths.Count; j++ )
            {
                // for each path inside the node show the count of that path
                Debug.Log("count of the length of the path: " + allNodes[i].GetComponent<NodeScript>().shortestPaths[j].Count);

                // for each node in the current shortests path list
                for (int k = 0; k < allNodes[i].GetComponent<NodeScript>().shortestPaths[j].Count; k++ )
                {
                    Debug.Log("Node Name: " + allNodes[i].GetComponent<NodeScript>().shortestPaths[j][k]);
                }
            }

        }
        */

        // all nodes have updated
        return true;
    }
    #endregion

    #region calcPathWeight

    /// <summary>
    /// NAME:
    ///     calcPathWeight
    /// DESCRIPTION:
    ///     Calculates the weight of the given path
    /// DEPENDENCIES:
    ///     Node
    /// NOTES:
    ///     returns as a float, if weights of nodes aren't floats, they'll be 
    ///     typecastd
    /// </summary>
    public static float calcPathWeight(List<GameObject> path)      // DONE! Booya! // target, target-1, source  
    {
        // Initialize variables
        float totalWeight = 0;
        GameObject tmpOriginNode;
        GameObject tmpTargetNode;

        // setup loop to go through path
        for (int pathIndex = 0; pathIndex < path.Count; pathIndex++) // target, target-1, source
        {
            // for each node 
            tmpOriginNode = path[pathIndex];  // the first node is target
            
                // see if target is the origin
                if ( tmpOriginNode == path[path.Count - 1] )
                {
                // this means we are at the last node in the path - Done!
                return totalWeight;
                }

            tmpTargetNode = path[pathIndex+1]; // grab next node

            // Add weight from current index to neighbor index
                // look through the firsts nodes list of neighbors for the target node and get the index of that target.
           int tmpIndexOfNextNodeWeight = tmpOriginNode.GetComponent<NodeScript>().neighbors.FindIndex(x => x == tmpTargetNode);

            // this is access of the origin nodes weight to the target node; added to the total weight
            totalWeight += (path[pathIndex].GetComponent<NodeScript>().neighborWeightList[tmpIndexOfNextNodeWeight]);    
        }

        return totalWeight;  // we will only get here if the last node is not the A node in the A calls: target, target-1, A-source node
    }

    #endregion

    #region SortByName
    /// <summary>
    /// NAME:
    ///     SortByName
    /// 
    /// 
    /// 
    /// </summary>
    /// <param name="unsortedList"></param>
    /// <returns></returns>
    public static List<GameObject> SortByName(List<GameObject> unsortedList)
    {
        
        for(int i = 0; i < unsortedList.Count-1; i++)
        {
            string[] tempString1 = unsortedList[i].name.Split(' ');
            string number = tempString1[1];

            unsortedList[i].transform.SetSiblingIndex(int.Parse(number)+1);
        }
        return unsortedList;
    }
    #endregion  

}